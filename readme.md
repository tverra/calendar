# Calendar App

## Installation
```
Compile and run: 
> [path to]\msbuild.exe Calendar\Calendar.csproj.
msbuild.exe is usually located in C:\Windows\Microsoft.NET\Framework\v4.0.30319\.
Go to output folder \Calendar\Calendar\bin\Debug and run Calendar.exe.
```

## Tests
```
Run tests:
> [path to]\vstest.console.exe" CalendarTests\bin\Debug\netcoreapp2.1\CalendarTests.dll

vstest.console.exe is located somwhere around 
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow\,
depending on your Visual Studio version.
```

## Source code
```
https://gitlab.com/tverra/calendar
```