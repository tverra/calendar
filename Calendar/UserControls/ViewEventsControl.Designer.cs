﻿namespace Calendar.UserControls
{
	partial class ViewEventsControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.EventsPanel = new System.Windows.Forms.TableLayoutPanel();
			this.DateRowLabel = new System.Windows.Forms.Label();
			this.DescriptionRowLabel = new System.Windows.Forms.Label();
			this.DurationRowLabel = new System.Windows.Forms.Label();
			this.EventsPanelHeader = new System.Windows.Forms.TableLayoutPanel();
			this.DescriptionLabel = new System.Windows.Forms.Label();
			this.DurationLabel = new System.Windows.Forms.Label();
			this.DateLabel = new System.Windows.Forms.Label();
			this.EventsPanel.SuspendLayout();
			this.EventsPanelHeader.SuspendLayout();
			this.SuspendLayout();
			// 
			// EventsPanel
			// 
			this.EventsPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.EventsPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.EventsPanel.ColumnCount = 4;
			this.EventsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.EventsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
			this.EventsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
			this.EventsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.EventsPanel.Controls.Add(this.DateRowLabel, 2, 0);
			this.EventsPanel.Controls.Add(this.DescriptionRowLabel, 0, 0);
			this.EventsPanel.Controls.Add(this.DurationRowLabel, 1, 0);
			this.EventsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.EventsPanel.Location = new System.Drawing.Point(0, 40);
			this.EventsPanel.Name = "EventsPanel";
			this.EventsPanel.RowCount = 2;
			this.EventsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.EventsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.EventsPanel.Size = new System.Drawing.Size(541, 369);
			this.EventsPanel.TabIndex = 5;
			// 
			// DateRowLabel
			// 
			this.DateRowLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DateRowLabel.AutoSize = true;
			this.DateRowLabel.Location = new System.Drawing.Point(332, 1);
			this.DateRowLabel.Name = "DateRowLabel";
			this.DateRowLabel.Size = new System.Drawing.Size(144, 40);
			this.DateRowLabel.TabIndex = 2;
			this.DateRowLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DescriptionRowLabel
			// 
			this.DescriptionRowLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DescriptionRowLabel.AutoSize = true;
			this.DescriptionRowLabel.Location = new System.Drawing.Point(4, 1);
			this.DescriptionRowLabel.Name = "DescriptionRowLabel";
			this.DescriptionRowLabel.Size = new System.Drawing.Size(170, 40);
			this.DescriptionRowLabel.TabIndex = 0;
			this.DescriptionRowLabel.Text = "No events";
			this.DescriptionRowLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DurationRowLabel
			// 
			this.DurationRowLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DurationRowLabel.AutoSize = true;
			this.DurationRowLabel.Location = new System.Drawing.Point(181, 1);
			this.DurationRowLabel.Name = "DurationRowLabel";
			this.DurationRowLabel.Size = new System.Drawing.Size(144, 40);
			this.DurationRowLabel.TabIndex = 1;
			this.DurationRowLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// EventsPanelHeader
			// 
			this.EventsPanelHeader.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.EventsPanelHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.EventsPanelHeader.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.EventsPanelHeader.ColumnCount = 4;
			this.EventsPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.EventsPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
			this.EventsPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
			this.EventsPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
			this.EventsPanelHeader.Controls.Add(this.DescriptionLabel, 0, 0);
			this.EventsPanelHeader.Controls.Add(this.DurationLabel, 1, 0);
			this.EventsPanelHeader.Controls.Add(this.DateLabel, 2, 0);
			this.EventsPanelHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.EventsPanelHeader.Location = new System.Drawing.Point(0, 0);
			this.EventsPanelHeader.Name = "EventsPanelHeader";
			this.EventsPanelHeader.RowCount = 1;
			this.EventsPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
			this.EventsPanelHeader.Size = new System.Drawing.Size(541, 40);
			this.EventsPanelHeader.TabIndex = 4;
			// 
			// DescriptionLabel
			// 
			this.DescriptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.DescriptionLabel.AutoSize = true;
			this.DescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DescriptionLabel.Location = new System.Drawing.Point(7, 7);
			this.DescriptionLabel.Margin = new System.Windows.Forms.Padding(6);
			this.DescriptionLabel.Name = "DescriptionLabel";
			this.DescriptionLabel.Size = new System.Drawing.Size(71, 31);
			this.DescriptionLabel.TabIndex = 0;
			this.DescriptionLabel.Text = "Description";
			this.DescriptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DurationLabel
			// 
			this.DurationLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.DurationLabel.AutoSize = true;
			this.DurationLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DurationLabel.Location = new System.Drawing.Point(184, 7);
			this.DurationLabel.Margin = new System.Windows.Forms.Padding(6);
			this.DurationLabel.Name = "DurationLabel";
			this.DurationLabel.Size = new System.Drawing.Size(55, 31);
			this.DurationLabel.TabIndex = 1;
			this.DurationLabel.Text = "Duration";
			this.DurationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DateLabel
			// 
			this.DateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.DateLabel.AutoSize = true;
			this.DateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DateLabel.Location = new System.Drawing.Point(335, 7);
			this.DateLabel.Margin = new System.Windows.Forms.Padding(6);
			this.DateLabel.Name = "DateLabel";
			this.DateLabel.Size = new System.Drawing.Size(34, 31);
			this.DateLabel.TabIndex = 2;
			this.DateLabel.Text = "Date";
			this.DateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ViewEventsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.EventsPanel);
			this.Controls.Add(this.EventsPanelHeader);
			this.Name = "ViewEventsControl";
			this.Size = new System.Drawing.Size(541, 409);
			this.EventsPanel.ResumeLayout(false);
			this.EventsPanel.PerformLayout();
			this.EventsPanelHeader.ResumeLayout(false);
			this.EventsPanelHeader.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel EventsPanel;
		private System.Windows.Forms.Label DateRowLabel;
		private System.Windows.Forms.Label DescriptionRowLabel;
		private System.Windows.Forms.Label DurationRowLabel;
		private System.Windows.Forms.TableLayoutPanel EventsPanelHeader;
		private System.Windows.Forms.Label DescriptionLabel;
		private System.Windows.Forms.Label DurationLabel;
		private System.Windows.Forms.Label DateLabel;
	}
}
