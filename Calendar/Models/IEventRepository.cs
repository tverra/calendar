﻿using System;
using System.Collections.Generic;

namespace Calendar.Models
{
	public interface IEventRepository
	{
		List<CalendarEvent> GetEvents(DateTime date);

		List<CalendarEvent> GetEvents(DateTime startDate, DateTime endDate);

		int Create(CalendarEvent calendarEvent);

		int Update(CalendarEvent calendarEvent);

		int Delete(CalendarEvent calendarEvent);
	}
}
