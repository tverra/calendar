using System;
using System.Collections.Generic;
using Calendar.Models;
using Calendar.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SQLite.Net;
using SQLite.Net.Platform.Win32;

namespace CalendarTests
{
	[TestClass]
	public class DataBaseTests
	{
		private IEventRepository _repository;
		private CalendarEvent _calendarEvent;

		[TestInitialize]
		public void SetupContext()
		{
			_calendarEvent = new CalendarEvent
			{
				Id = 0,
				Description = "Description",
				Date = DateTime.Now,
				StartTime = DateTime.Now.AddHours(1),
				EndTime = DateTime.Now.AddHours(2)
			};

			var db = new SQLiteConnection(new SQLitePlatformWin32(), ":memory:");
			db.CreateTable<CalendarEvent>();
			_repository = new EventRepository(db);
		}

		[TestMethod]
		public void GetEventsReturnsEventList()
		{
			_repository.Create(_calendarEvent);

			var queriedEvents = _repository.GetEvents(_calendarEvent.Date);

			Assert.IsInstanceOfType(queriedEvents, typeof(List<CalendarEvent>), "The return type is wrong");
		}

		[TestMethod]
		public void GetEventsOnEmptyDatabaseReturnsEmptySet()
		{
			Assert.AreEqual(0, _repository.GetEvents(DateTime.Now).Count, "The list is not empty");
		}

		[TestMethod]
		public void GetEventsReturnsEventsForCurrentDate()
		{
			var now = DateTime.Now;

			var events = new List<CalendarEvent>
			{
				new CalendarEvent("", now, now, DateTime.Now.AddDays(-2)),
				new CalendarEvent("", now, now, DateTime.Now.AddDays(-1)),
				new CalendarEvent("", now, now, now),
				new CalendarEvent("", now, now, now),
				new CalendarEvent("", now, now, now),
				new CalendarEvent("", now, now, DateTime.Now.AddDays(1)),
				new CalendarEvent("", now, now, DateTime.Now.AddDays(2))
			};

			foreach (var calendarEvent in events)
			{
				_repository.Create(calendarEvent);
			}

			var queriedEvents = _repository.GetEvents(now);
			Assert.AreEqual(3, queriedEvents.Count, "Wrong number of events for the given date");
		}

		[TestMethod]
		public void GetEventsDoesNotMatchOnTime()
		{
			var now = DateTime.Now;
			var startTime = DateTime.Now.AddHours(-1);
			var endTime = DateTime.Now.AddHours(1);
			var dateWithTimeOffset = DateTime.Now;

			_repository.Create(new CalendarEvent("Description", startTime, endTime, now));

			dateWithTimeOffset = DateTime.Now.AddHours(1).Date == now.Date ? dateWithTimeOffset.AddHours(1) : dateWithTimeOffset.AddHours(-1);

			var queriedEvents = _repository.GetEvents(dateWithTimeOffset);

			Assert.AreEqual(1, queriedEvents.Count, "Matches are not independent of time");
		}

		[TestMethod]
		public void GetEventRangeReturnsEventList()
		{
			_repository.Create(_calendarEvent);

			var queriedEvents = _repository.GetEvents(_calendarEvent.Date, _calendarEvent.Date);

			Assert.IsInstanceOfType(queriedEvents, typeof(List<CalendarEvent>), "The return type is wrong");
		}

		[TestMethod]
		public void GetEventRangeOnEmptyDatabaseReturnsEmptySet()
		{
			Assert.AreEqual(0, _repository.GetEvents(DateTime.MinValue, DateTime.MaxValue).Count, "The list is not empty");
		}

		[TestMethod]
		public void GetEventRangeReturnsEventsForCurrentDate()
		{
			var now = DateTime.Now;

			var events = new List<CalendarEvent>
			{
				new CalendarEvent("", now, now, DateTime.Now.AddDays(-2)),
				new CalendarEvent("", now, now, DateTime.Now.AddDays(-1)),
				new CalendarEvent("", now, now, now),
				new CalendarEvent("", now, now, now),
				new CalendarEvent("", now, now, now),
				new CalendarEvent("", now, now, DateTime.Now.AddDays(1)),
				new CalendarEvent("", now, now, DateTime.Now.AddDays(2))
			};

			foreach (var calendarEvent in events)
			{
				_repository.Create(calendarEvent);
			}

			var queriedEvents = _repository.GetEvents(now.AddDays(-1), now.AddDays(1));
			Assert.AreEqual(5, queriedEvents.Count, "Wrong number of events for the given date");
		}

		[TestMethod]
		public void GetEventRangeDoesNotMatchOnTime()
		{
			_repository.Create(_calendarEvent);

			var queriedEvents = _repository.GetEvents(DateTime.Now.Date, DateTime.Now.Date);

			Assert.AreEqual(1, queriedEvents.Count, "Matches are not independent of time");
		}

		[TestMethod]
		public void GetEventRangeAreReturnedByDate()
		{
			var date = DateTime.Now;
			var startTime = DateTime.Now.AddHours(-1);
			var endTime = DateTime.Now.AddHours(1);

			var list = new List<CalendarEvent>();

			list.Add(new CalendarEvent("Description", startTime, endTime, date));
			date = date.AddDays(1);
			list.Add(new CalendarEvent("Description", startTime, endTime, date));
			date = date.AddDays(1);
			list.Add(new CalendarEvent("Description", startTime, endTime, date));
			date = date.AddDays(1);
			list.Add(new CalendarEvent("Description", startTime, endTime, date));
			date = date.AddDays(1);
			list.Add(new CalendarEvent("Description", startTime, endTime, date));

			_repository.Create(list[2]);
			_repository.Create(list[0]);
			_repository.Create(list[4]);
			_repository.Create(list[1]);
			_repository.Create(list[3]);

			var events = _repository.GetEvents(DateTime.Now, date);

			for (var i = 0; i < list.Count; i++)
			{
				Assert.AreEqual(list[i].Date, events[i].Date);
			}
		}

		[TestMethod]
		public void CreateEventInsertsEvent()
		{
			_repository.Create(_calendarEvent);

			var queriedEvents = _repository.GetEvents(_calendarEvent.Date);

			Assert.AreEqual(1, queriedEvents.Count, "The event is not inserted");
		}

		[TestMethod]
		public void CreateEventsInsertCorrectAmount()
		{
			for (var i = 0; i < 20; i++)
			{
				_repository.Create(_calendarEvent);
			}

			var queriedEvents = _repository.GetEvents(_calendarEvent.Date);

			Assert.AreEqual(20, queriedEvents.Count, "The number of inserted events is not correct");
		}

		[TestMethod]
		public void CreateEventInsertsCorrectData()
		{
			var now = DateTime.Now;
			var startTime = DateTime.Now.AddHours(-1);
			var endTime = DateTime.Now.AddHours(1);

			_repository.Create(new CalendarEvent("Description", startTime, endTime, now));

			var calendarEvent = _repository.GetEvents(now)[0];

			Assert.AreEqual(1, calendarEvent.Id, "Wrong id is inserted");
			Assert.AreEqual("Description", calendarEvent.Description, "Wrong description is inserted");
			Assert.AreEqual(now, calendarEvent.Date, "Wrong date is inserted");
			Assert.AreEqual(startTime, calendarEvent.StartTime.ToLocalTime(), "Wrong start time is inserted");
			Assert.AreEqual(endTime, calendarEvent.EndTime.ToLocalTime(), "Wrong end time is inserted");
		}

		[TestMethod]
		public void GetEventRangeReturnsCorrectData()
		{
			var now = DateTime.Now;
			var startTime = DateTime.Now.AddHours(-1);
			var endTime = DateTime.Now.AddHours(1);

			_repository.Create(new CalendarEvent("Description", startTime, endTime, now));

			var calendarEvent = _repository.GetEvents(now, now)[0];

			Assert.AreEqual(1, calendarEvent.Id, "Wrong id is inserted");
			Assert.AreEqual("Description", calendarEvent.Description, "Wrong description is inserted");
			Assert.AreEqual(now, calendarEvent.Date, "Wrong date is inserted");
			Assert.AreEqual(startTime, calendarEvent.StartTime.ToLocalTime(), "Wrong start time is inserted");
			Assert.AreEqual(endTime, calendarEvent.EndTime.ToLocalTime(), "Wrong end time is inserted");
		}

		[TestMethod]
		public void UpdateEventUpdatesCorrectData()
		{
			var now = DateTime.Now;
			var startTime = DateTime.Now.AddHours(-1);
			var endTime = DateTime.Now.AddHours(1);

			_repository.Create(new CalendarEvent("Description", startTime, endTime, now));

			now = now.AddDays(1);
			startTime = startTime.AddDays(1);
			endTime = endTime.AddDays(1);

			var calendarEvent = _repository.GetEvents(DateTime.Now, DateTime.Now.AddDays(1))[0];
			calendarEvent.Description = "New description";
			calendarEvent.Date = now;
			calendarEvent.StartTime = startTime;
			calendarEvent.EndTime = endTime;
			_repository.Update(calendarEvent);

			calendarEvent = _repository.GetEvents(DateTime.Now, DateTime.Now.AddDays(1))[0];

			Assert.AreEqual(1, calendarEvent.Id, "Wrong id is inserted");
			Assert.AreEqual("New description", calendarEvent.Description, "Wrong description is inserted");
			Assert.AreEqual(now, calendarEvent.Date, "Wrong date is inserted");
			Assert.AreEqual(startTime, calendarEvent.StartTime.ToLocalTime(), "Wrong start time is inserted");
			Assert.AreEqual(endTime, calendarEvent.EndTime.ToLocalTime(), "Wrong end time is inserted");
		}

		[TestMethod]
		public void DeleteEventDeletesEvent()
		{
			_repository.Create(_calendarEvent);
			var calendarEvent = _repository.GetEvents(DateTime.MinValue, DateTime.MaxValue)[0];
			_repository.Delete(calendarEvent);

			Assert.AreEqual(0, _repository.GetEvents(DateTime.MinValue, DateTime.MaxValue).Count, "The event was not deleted");
		}
	}
}
