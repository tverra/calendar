﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Calendar.Models;
using Calendar.Utils;

namespace Calendar.UserControls
{
	public partial class ViewEventsControl : UserControl
	{
		private readonly IEventRepository _repository = new EventRepository();

		public ViewEventsControl()
		{
			InitializeComponent();
		}

		public void SetEventDisplayDate(DateTime date)
		{
			UpdateEventList(date);
		}

		public void SetEventDisplayDate(DateTime startDate, DateTime endDate)
		{
			UpdateEventList(startDate, endDate);
		}

		private void UpdateEventList(DateTime date)
		{
			var events = _repository.GetEvents(date);
			SetEventListElements(events, date);
		}

		private void UpdateEventList(DateTime startDate, DateTime endDate)
		{
			var events = _repository.GetEvents(startDate, endDate);
			SetEventListElements(events, startDate, endDate);
		}

		private void SetEventListElements(IReadOnlyList<CalendarEvent> events,
			DateTime startDate, DateTime? endDate = null)
		{
			var design = new DesignUtils();
			EventsPanel.Controls.Clear();

			if (events.Count == 0)
			{
				SetEmptyRow();
				return;
			}

			EventsPanel.RowCount = events.Count + 1;

			for (var i = 0; i < events.Count; i++)
			{
				var calendarEvent = events[i];
				var deleteButton = design.GetEventDeleteButton(i);
				deleteButton.Click += (s, e) =>
				{
					_repository.Delete(calendarEvent);

					if (endDate == null)
						UpdateEventList(startDate);
					else
						UpdateEventList(startDate, (DateTime) endDate);
				};

				EventsPanel.Controls.Add(
					design.GetEventDescriptionRow(calendarEvent.Description, i),
					0, i);
				EventsPanel.Controls.Add(
					design.GetEventDurationLabel(calendarEvent.StartTime, calendarEvent.EndTime, i),
					1, i);
				EventsPanel.Controls.Add(
					design.GetEventDateLabel(calendarEvent.Date, i),
					2, i);
				EventsPanel.Controls.Add(deleteButton, 3, i);
			}
		}

		private void SetEmptyRow()
		{
			EventsPanel.Controls.Add(DateRowLabel, 2, 0);
			EventsPanel.Controls.Add(DescriptionRowLabel, 0, 0);
			EventsPanel.Controls.Add(DurationRowLabel, 1, 0);
			EventsPanel.RowCount = 2;
		}
	}
}
