﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calendar.Data;
using SQLite.Net;

namespace Calendar.Models
{
	public class EventRepository : IEventRepository
	{
		private readonly SQLiteConnection _db;

		public EventRepository()
		{
			var dbUtils = new DataBaseUtils();
			_db = dbUtils.GetDbConnection();
		}

		public EventRepository(SQLiteConnection db)
		{
			_db = db;
		}

		public List<CalendarEvent> GetEvents(DateTime date)
		{
			date = date.ToUniversalTime();
			var result = _db.Table<CalendarEvent>().ToList().
				Where(e => e.Date.Date == date.Date).OrderBy(e => e.Date).ToList();
			ConvertToLocalTime(result);
			return result;
		}

		public List<CalendarEvent> GetEvents(DateTime startDate, DateTime endDate)
		{
			startDate = startDate.ToUniversalTime();
			endDate = endDate.ToLocalTime();
			var result = _db.Table<CalendarEvent>().ToList()
				.Where(e => (e.Date.Date >= startDate.Date && e.Date.Date <= endDate.Date))
				.OrderBy(e => e.Date).ToList();
			ConvertToLocalTime(result);
			return result;
		}

		public int Create(CalendarEvent calendarEvent)
		{
			return _db.Insert(calendarEvent);
		}

		public int Update(CalendarEvent calendarEvent)
		{
			return _db.Update(calendarEvent);
		}

		public int Delete(CalendarEvent calendarEvent)
		{
			return _db.Delete(calendarEvent);
		}

		private void ConvertToLocalTime(IEnumerable<CalendarEvent> calendarEvents)
		{
			foreach (var calendarEvent in calendarEvents)
			{
				ConvertToLocalTime(calendarEvent);
			}
		}

		private void ConvertToLocalTime(CalendarEvent calendarEvent)
		{
			calendarEvent.Date = calendarEvent.Date.ToLocalTime();
			calendarEvent.StartTime = calendarEvent.StartTime.ToLocalTime();
			calendarEvent.EndTime = calendarEvent.EndTime.ToLocalTime();
		}
	}
}
