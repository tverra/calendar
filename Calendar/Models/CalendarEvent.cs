﻿using System;
using SQLite.Net.Attributes;

namespace Calendar.Models
{
	public class CalendarEvent
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }
		[NotNull]
		public string Description { get; set; }
		[NotNull]
		public DateTime StartTime { get; set; }
		[NotNull]
		public DateTime EndTime { get; set; }
		[NotNull]
		public DateTime Date { get; set; }

		public CalendarEvent()
		{

		}

		public CalendarEvent(string description, DateTime startTime, DateTime endTime, DateTime date)
		{
			Description = description;
			StartTime = startTime;
			EndTime = endTime;
			Date = date;
		}
	}
}
