﻿using Calendar.Models;
using SQLite.Net;
using SQLite.Net.Platform.Win32;

namespace Calendar.Data
{
	public class DataBaseUtils
	{
		private const string DbName = "CalendarDB.sqlite";

		public void InitDataBase()
		{
			var db = new SQLiteConnection(new SQLitePlatformWin32(), GetDbName(), false);
			db.CreateTable<CalendarEvent>();
		}

		public string GetDbName()
		{
			return DbName;
		}

		public SQLiteConnection GetDbConnection()
		{
			return new SQLiteConnection(new SQLitePlatformWin32(), GetDbName(), false);
		}

		public SQLiteConnection GetDbConnection(string dbName)
		{
			return new SQLiteConnection(new SQLitePlatformWin32(), dbName, false);
		}
	}
}
