﻿using System;
using System.Windows.Forms;
using Calendar.Models;

namespace Calendar.UserControls
{
	public partial class CreateEventControl : UserControl
	{
		public CreateEventControl()
		{
			InitializeComponent();

			SetTimePickerFormat(StartTimePicker);
			SetTimePickerFormat(EndTimePicker);
			SetDatePickerFormat(DatePicker);
			FormatInputFields();
		}

		private void CancelButton_Click(object sender, EventArgs e)
		{
			Parent.Controls.Remove(this);
		}

		private void SetTimePickerFormat(DateTimePicker dateTimePicker)
		{
			dateTimePicker.Format = DateTimePickerFormat.Custom;
			dateTimePicker.CustomFormat = "HH:mm";
			dateTimePicker.ShowUpDown = true;
		}

		private void SetDatePickerFormat(DateTimePicker dateTimePicker)
		{
			dateTimePicker.Format = DateTimePickerFormat.Custom;
			dateTimePicker.CustomFormat = "yyyy-MM-dd";
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			var calendarEvent = new CalendarEvent
			{
				Description = DescriptionTextBox.Text,
				StartTime = StartTimePicker.Value,
				EndTime = EndTimePicker.Value,
				Date = DatePicker.Value
			};

			IEventRepository repository = new EventRepository();
			repository.Create(calendarEvent);

			FormatInputFields();
		}

		private void FormatInputFields()
		{
			var hourNow = DateTime.Now.Date;
			var hourStart = hourNow.AddHours(DateTime.Now.Hour);
			var hourEnd = hourNow.AddHours(DateTime.Now.Hour);
			hourEnd = hourEnd.AddHours(1);

			DescriptionTextBox.Text = "";
			StartTimePicker.Value = hourStart;
			EndTimePicker.Value = hourEnd;
			DatePicker.Value = DateTime.Now;
		}
	}
}
