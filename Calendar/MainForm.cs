﻿using System;
using System.Windows.Forms;
using Calendar.Data;
using Calendar.UserControls;

namespace Calendar
{
	public partial class MainForm : Form
	{
		private readonly ViewEventsControl _viewEventsControl = new ViewEventsControl { Dock = DockStyle.Fill };
		private readonly CreateEventControl _editEventControl = new CreateEventControl { Dock = DockStyle.Fill };

		public MainForm()
		{
			InitializeComponent();

			var dbUtils = new DataBaseUtils();
			dbUtils.InitDataBase();

			var cancelButton = _editEventControl.Controls.Find("CancelButton", true)[0];
			cancelButton.Click += CancelButton_Click;

			var saveButton = _editEventControl.Controls.Find("SaveButton", true)[0];
			saveButton.Click += SaveButton_Click;

			SetDatePickerFormat(DisplayEventsStartDatePicker);
			SetDatePickerFormat(DisplayEventsEndDatePicker);

			_viewEventsControl.SetEventDisplayDate(DateTime.Now);
			ControlContainer.Controls.Add(_viewEventsControl);
		}

		private void MonthCalendar_DateChanged(object sender, DateRangeEventArgs e)
		{
			if (! DisplayForSelectedDateButton.Checked)
			{
				return;
			}

			SetMonthCalendarDateRange();
		}

		private void SetMonthCalendarDateRange()
		{
			var dateStart = AddTimeToDate(MonthCalendar.SelectionStart);
			var dateEnd = AddTimeToDate(MonthCalendar.SelectionStart);
			_viewEventsControl.SetEventDisplayDate(dateStart, dateEnd);
		}

		private void NewEventButton_Click(object sender, EventArgs e)
		{
			ControlContainer.Controls.Clear();
			ControlContainer.Controls.Add(_editEventControl);
		}

		private void CancelButton_Click(object sender, EventArgs e)
		{
			ControlContainer.Controls.Clear();
			ControlContainer.Controls.Add(_viewEventsControl);
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			ControlContainer.Controls.Clear();
			ControlContainer.Controls.Add(_viewEventsControl);
			UpdateEventList();
		}

		private void SetDatePickerFormat(DateTimePicker dateTimePicker)
		{
			dateTimePicker.Format = DateTimePickerFormat.Custom;
			dateTimePicker.CustomFormat = "yyyy-MM-dd";
		}

		private void DisplayForDateRangeButton_CheckedChanged(object sender, EventArgs e)
		{
			DisplayEventsStartDatePicker.Enabled = DisplayForDateRangeButton.Checked;
			DisplayEventsEndDatePicker.Enabled = DisplayForDateRangeButton.Checked;

			UpdateEventList();
		}

		private void UpdateEventList()
		{
			if (DisplayForDateRangeButton.Checked)
			{
				_viewEventsControl.SetEventDisplayDate(
					DisplayEventsStartDatePicker.Value,
					DisplayEventsEndDatePicker.Value);
			}
			else
			{
				SetMonthCalendarDateRange();
			}
		}

		private DateTime AddTimeToDate(DateTime dateTime)
		{
			dateTime = dateTime.AddHours(DateTime.Now.Hour);
			dateTime = dateTime.AddMinutes(DateTime.Now.Minute);
			dateTime = dateTime.AddSeconds(DateTime.Now.Second);
			return dateTime;
		}

		private void DisplayEventsStartDatePicker_ValueChanged(object sender, EventArgs e)
		{
			_viewEventsControl.SetEventDisplayDate(
				DisplayEventsStartDatePicker.Value,
				DisplayEventsEndDatePicker.Value);
		}

		private void DisplayEventsEndDatePicker_ValueChanged(object sender, EventArgs e)
		{
			_viewEventsControl.SetEventDisplayDate(
				DisplayEventsStartDatePicker.Value,
				DisplayEventsEndDatePicker.Value);
		}
	}
}
