﻿namespace Calendar
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CalendarPanel = new System.Windows.Forms.Panel();
			this.MonthCalendar = new System.Windows.Forms.MonthCalendar();
			this.MenuPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.SortingChoicePanel = new System.Windows.Forms.TableLayoutPanel();
			this.DisplayForSelectedDateButton = new System.Windows.Forms.RadioButton();
			this.DisplayForDateRangeButton = new System.Windows.Forms.RadioButton();
			this.HyphenLabel = new System.Windows.Forms.Label();
			this.DisplayEventsStartDatePicker = new System.Windows.Forms.DateTimePicker();
			this.DisplayEventsEndDatePicker = new System.Windows.Forms.DateTimePicker();
			this.NewEventButton = new System.Windows.Forms.Button();
			this.ControlContainer = new System.Windows.Forms.Panel();
			this.CalendarPanel.SuspendLayout();
			this.MenuPanel.SuspendLayout();
			this.SortingChoicePanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// CalendarPanel
			// 
			this.CalendarPanel.BackColor = System.Drawing.SystemColors.Window;
			this.CalendarPanel.Controls.Add(this.MonthCalendar);
			this.CalendarPanel.Dock = System.Windows.Forms.DockStyle.Left;
			this.CalendarPanel.Location = new System.Drawing.Point(0, 0);
			this.CalendarPanel.Name = "CalendarPanel";
			this.CalendarPanel.Padding = new System.Windows.Forms.Padding(9);
			this.CalendarPanel.Size = new System.Drawing.Size(259, 479);
			this.CalendarPanel.TabIndex = 0;
			// 
			// MonthCalendar
			// 
			this.MonthCalendar.CalendarDimensions = new System.Drawing.Size(1, 3);
			this.MonthCalendar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MonthCalendar.Location = new System.Drawing.Point(9, 9);
			this.MonthCalendar.Name = "MonthCalendar";
			this.MonthCalendar.TabIndex = 0;
			this.MonthCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.MonthCalendar_DateChanged);
			// 
			// MenuPanel
			// 
			this.MenuPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.MenuPanel.Controls.Add(this.SortingChoicePanel);
			this.MenuPanel.Controls.Add(this.NewEventButton);
			this.MenuPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.MenuPanel.Location = new System.Drawing.Point(259, 0);
			this.MenuPanel.Name = "MenuPanel";
			this.MenuPanel.Size = new System.Drawing.Size(541, 70);
			this.MenuPanel.TabIndex = 1;
			// 
			// SortingChoicePanel
			// 
			this.SortingChoicePanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.SortingChoicePanel.ColumnCount = 4;
			this.SortingChoicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.SortingChoicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.SortingChoicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.SortingChoicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.SortingChoicePanel.Controls.Add(this.DisplayForSelectedDateButton, 0, 0);
			this.SortingChoicePanel.Controls.Add(this.DisplayForDateRangeButton, 0, 1);
			this.SortingChoicePanel.Controls.Add(this.HyphenLabel, 2, 1);
			this.SortingChoicePanel.Controls.Add(this.DisplayEventsStartDatePicker, 1, 1);
			this.SortingChoicePanel.Controls.Add(this.DisplayEventsEndDatePicker, 3, 1);
			this.SortingChoicePanel.Location = new System.Drawing.Point(9, 9);
			this.SortingChoicePanel.Margin = new System.Windows.Forms.Padding(9);
			this.SortingChoicePanel.Name = "SortingChoicePanel";
			this.SortingChoicePanel.RowCount = 2;
			this.SortingChoicePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.SortingChoicePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.SortingChoicePanel.Size = new System.Drawing.Size(405, 49);
			this.SortingChoicePanel.TabIndex = 0;
			// 
			// DisplayForSelectedDateButton
			// 
			this.DisplayForSelectedDateButton.AutoSize = true;
			this.DisplayForSelectedDateButton.Checked = true;
			this.DisplayForSelectedDateButton.Location = new System.Drawing.Point(3, 3);
			this.DisplayForSelectedDateButton.Name = "DisplayForSelectedDateButton";
			this.DisplayForSelectedDateButton.Size = new System.Drawing.Size(141, 17);
			this.DisplayForSelectedDateButton.TabIndex = 0;
			this.DisplayForSelectedDateButton.TabStop = true;
			this.DisplayForSelectedDateButton.Text = "Display for selected date";
			this.DisplayForSelectedDateButton.UseVisualStyleBackColor = true;
			// 
			// DisplayForDateRangeButton
			// 
			this.DisplayForDateRangeButton.AutoSize = true;
			this.DisplayForDateRangeButton.Location = new System.Drawing.Point(3, 27);
			this.DisplayForDateRangeButton.Name = "DisplayForDateRangeButton";
			this.DisplayForDateRangeButton.Size = new System.Drawing.Size(109, 17);
			this.DisplayForDateRangeButton.TabIndex = 1;
			this.DisplayForDateRangeButton.Text = "Display for period:";
			this.DisplayForDateRangeButton.UseVisualStyleBackColor = true;
			this.DisplayForDateRangeButton.CheckedChanged += new System.EventHandler(this.DisplayForDateRangeButton_CheckedChanged);
			// 
			// HyphenLabel
			// 
			this.HyphenLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.HyphenLabel.AutoSize = true;
			this.HyphenLabel.Location = new System.Drawing.Point(271, 24);
			this.HyphenLabel.Name = "HyphenLabel";
			this.HyphenLabel.Size = new System.Drawing.Size(10, 25);
			this.HyphenLabel.TabIndex = 2;
			this.HyphenLabel.Text = "-";
			this.HyphenLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// DisplayEventsStartDatePicker
			// 
			this.DisplayEventsStartDatePicker.Enabled = false;
			this.DisplayEventsStartDatePicker.Location = new System.Drawing.Point(150, 27);
			this.DisplayEventsStartDatePicker.Name = "DisplayEventsStartDatePicker";
			this.DisplayEventsStartDatePicker.Size = new System.Drawing.Size(115, 20);
			this.DisplayEventsStartDatePicker.TabIndex = 3;
			this.DisplayEventsStartDatePicker.ValueChanged += new System.EventHandler(this.DisplayEventsStartDatePicker_ValueChanged);
			// 
			// DisplayEventsEndDatePicker
			// 
			this.DisplayEventsEndDatePicker.Enabled = false;
			this.DisplayEventsEndDatePicker.Location = new System.Drawing.Point(287, 27);
			this.DisplayEventsEndDatePicker.Name = "DisplayEventsEndDatePicker";
			this.DisplayEventsEndDatePicker.Size = new System.Drawing.Size(115, 20);
			this.DisplayEventsEndDatePicker.TabIndex = 4;
			this.DisplayEventsEndDatePicker.ValueChanged += new System.EventHandler(this.DisplayEventsEndDatePicker_ValueChanged);
			// 
			// NewEventButton
			// 
			this.NewEventButton.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.NewEventButton.Location = new System.Drawing.Point(432, 9);
			this.NewEventButton.Margin = new System.Windows.Forms.Padding(9);
			this.NewEventButton.Name = "NewEventButton";
			this.NewEventButton.Size = new System.Drawing.Size(100, 53);
			this.NewEventButton.TabIndex = 1;
			this.NewEventButton.Text = "New event";
			this.NewEventButton.UseVisualStyleBackColor = false;
			this.NewEventButton.Click += new System.EventHandler(this.NewEventButton_Click);
			// 
			// ControlContainer
			// 
			this.ControlContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ControlContainer.Location = new System.Drawing.Point(259, 70);
			this.ControlContainer.Name = "ControlContainer";
			this.ControlContainer.Size = new System.Drawing.Size(541, 409);
			this.ControlContainer.TabIndex = 2;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 479);
			this.Controls.Add(this.ControlContainer);
			this.Controls.Add(this.MenuPanel);
			this.Controls.Add(this.CalendarPanel);
			this.MinimumSize = new System.Drawing.Size(816, 518);
			this.Name = "MainForm";
			this.Text = "Calendar";
			this.CalendarPanel.ResumeLayout(false);
			this.MenuPanel.ResumeLayout(false);
			this.SortingChoicePanel.ResumeLayout(false);
			this.SortingChoicePanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel CalendarPanel;
		private System.Windows.Forms.MonthCalendar MonthCalendar;
		private System.Windows.Forms.FlowLayoutPanel MenuPanel;
		private System.Windows.Forms.TableLayoutPanel SortingChoicePanel;
		private System.Windows.Forms.RadioButton DisplayForSelectedDateButton;
		private System.Windows.Forms.RadioButton DisplayForDateRangeButton;
		private System.Windows.Forms.Label HyphenLabel;
		private System.Windows.Forms.Button NewEventButton;
		private System.Windows.Forms.Panel ControlContainer;
		private System.Windows.Forms.DateTimePicker DisplayEventsStartDatePicker;
		private System.Windows.Forms.DateTimePicker DisplayEventsEndDatePicker;
	}
}

