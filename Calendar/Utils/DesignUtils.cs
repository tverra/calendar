﻿using System;
using System.Windows.Forms;

namespace Calendar.Utils
{
	public class DesignUtils
	{
		public Label GetEventDescriptionRow(string description, int rowIndex)
		{
			return new Label
			{
				Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
				AutoSize = true,
				Location = new System.Drawing.Point(4, 1),
				Name = "DescriptionRowLabel" + rowIndex,
				Size = new System.Drawing.Size(108, 40),
				TabIndex = 0,
				Text = description,
				TextAlign = System.Drawing.ContentAlignment.MiddleLeft
			};
		}

		public Label GetEventDurationLabel(DateTime startTime, DateTime endTime, int rowIndex)
		{
			return new Label
			{
				Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
				AutoSize = true,
				Location = new System.Drawing.Point(119, 1),
				Name = "DurationRowLabel" + rowIndex,
				Size = new System.Drawing.Size(144, 40),
				TabIndex = 1,
				Text = startTime.ToShortTimeString() + " - " +
				       endTime.ToShortTimeString(),
				TextAlign = System.Drawing.ContentAlignment.MiddleLeft
			};
		}

		public Label GetEventDateLabel(DateTime date, int rowIndex)
		{
			return new Label
			{
				Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right,
				AutoSize = true,
				Location = new System.Drawing.Point(270, 1),
				Name = "DateRowLabel" + rowIndex,
				Size = new System.Drawing.Size(144, 40),
				TabIndex = 2,
				Text = date.ToShortDateString(),
				TextAlign = System.Drawing.ContentAlignment.MiddleLeft
			};
		}

		public Button GetEventDeleteButton(int rowIndex)
		{
			return new Button
			{
				BackColor = System.Drawing.SystemColors.ControlLightLight,
				Location = new System.Drawing.Point(482, 4),
				Name = "DeleteRowButton" + rowIndex,
				Size = new System.Drawing.Size(54, 34),
				TabIndex = 4,
				Text = "Delete",
				UseVisualStyleBackColor = false
			};
		}
	}
}
