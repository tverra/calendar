﻿namespace Calendar.UserControls
{
	partial class CreateEventControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.CancelButton = new System.Windows.Forms.Button();
			this.SaveButton = new System.Windows.Forms.Button();
			this.NewEventInputContainer = new System.Windows.Forms.TableLayoutPanel();
			this.NewEventStartLabel = new System.Windows.Forms.Label();
			this.NewEventEndLabel = new System.Windows.Forms.Label();
			this.DescriptionLabel = new System.Windows.Forms.Label();
			this.DateLabel = new System.Windows.Forms.Label();
			this.DescriptionTextBox = new System.Windows.Forms.TextBox();
			this.StartTimePicker = new System.Windows.Forms.DateTimePicker();
			this.EndTimePicker = new System.Windows.Forms.DateTimePicker();
			this.DatePicker = new System.Windows.Forms.DateTimePicker();
			this.EventsPanelHeader = new System.Windows.Forms.TableLayoutPanel();
			this.NewEventLabel = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			this.NewEventInputContainer.SuspendLayout();
			this.EventsPanelHeader.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.CancelButton, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.SaveButton, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 369);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(541, 40);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// CancelButton
			// 
			this.CancelButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.CancelButton.Location = new System.Drawing.Point(3, 3);
			this.CancelButton.Name = "CancelButton";
			this.CancelButton.Size = new System.Drawing.Size(100, 34);
			this.CancelButton.TabIndex = 0;
			this.CancelButton.Text = "Cancel";
			this.CancelButton.UseVisualStyleBackColor = false;
			this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// SaveButton
			// 
			this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.SaveButton.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.SaveButton.Location = new System.Drawing.Point(438, 3);
			this.SaveButton.Name = "SaveButton";
			this.SaveButton.Size = new System.Drawing.Size(100, 34);
			this.SaveButton.TabIndex = 1;
			this.SaveButton.Text = "Save";
			this.SaveButton.UseVisualStyleBackColor = false;
			this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
			// 
			// NewEventInputContainer
			// 
			this.NewEventInputContainer.ColumnCount = 2;
			this.NewEventInputContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.NewEventInputContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.NewEventInputContainer.Controls.Add(this.NewEventStartLabel, 0, 1);
			this.NewEventInputContainer.Controls.Add(this.NewEventEndLabel, 0, 2);
			this.NewEventInputContainer.Controls.Add(this.DescriptionLabel, 0, 0);
			this.NewEventInputContainer.Controls.Add(this.DateLabel, 0, 3);
			this.NewEventInputContainer.Controls.Add(this.DescriptionTextBox, 1, 0);
			this.NewEventInputContainer.Controls.Add(this.StartTimePicker, 1, 1);
			this.NewEventInputContainer.Controls.Add(this.EndTimePicker, 1, 2);
			this.NewEventInputContainer.Controls.Add(this.DatePicker, 1, 3);
			this.NewEventInputContainer.Location = new System.Drawing.Point(0, 46);
			this.NewEventInputContainer.Name = "NewEventInputContainer";
			this.NewEventInputContainer.RowCount = 5;
			this.NewEventInputContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.NewEventInputContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.NewEventInputContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.NewEventInputContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
			this.NewEventInputContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.NewEventInputContainer.Size = new System.Drawing.Size(541, 323);
			this.NewEventInputContainer.TabIndex = 1;
			// 
			// NewEventStartLabel
			// 
			this.NewEventStartLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.NewEventStartLabel.AutoSize = true;
			this.NewEventStartLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NewEventStartLabel.Location = new System.Drawing.Point(6, 46);
			this.NewEventStartLabel.Margin = new System.Windows.Forms.Padding(6);
			this.NewEventStartLabel.Name = "NewEventStartLabel";
			this.NewEventStartLabel.Size = new System.Drawing.Size(34, 28);
			this.NewEventStartLabel.TabIndex = 1;
			this.NewEventStartLabel.Text = "Start";
			// 
			// NewEventEndLabel
			// 
			this.NewEventEndLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.NewEventEndLabel.AutoSize = true;
			this.NewEventEndLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NewEventEndLabel.Location = new System.Drawing.Point(6, 86);
			this.NewEventEndLabel.Margin = new System.Windows.Forms.Padding(6);
			this.NewEventEndLabel.Name = "NewEventEndLabel";
			this.NewEventEndLabel.Size = new System.Drawing.Size(29, 28);
			this.NewEventEndLabel.TabIndex = 2;
			this.NewEventEndLabel.Text = "End";
			// 
			// DescriptionLabel
			// 
			this.DescriptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.DescriptionLabel.AutoSize = true;
			this.DescriptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DescriptionLabel.Location = new System.Drawing.Point(6, 6);
			this.DescriptionLabel.Margin = new System.Windows.Forms.Padding(6);
			this.DescriptionLabel.Name = "DescriptionLabel";
			this.DescriptionLabel.Size = new System.Drawing.Size(71, 28);
			this.DescriptionLabel.TabIndex = 4;
			this.DescriptionLabel.Text = "Description";
			// 
			// DateLabel
			// 
			this.DateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.DateLabel.AutoSize = true;
			this.DateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DateLabel.Location = new System.Drawing.Point(6, 126);
			this.DateLabel.Margin = new System.Windows.Forms.Padding(6);
			this.DateLabel.Name = "DateLabel";
			this.DateLabel.Size = new System.Drawing.Size(34, 28);
			this.DateLabel.TabIndex = 5;
			this.DateLabel.Text = "Date";
			// 
			// DescriptionTextBox
			// 
			this.DescriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DescriptionTextBox.Location = new System.Drawing.Point(86, 3);
			this.DescriptionTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 9, 3);
			this.DescriptionTextBox.Name = "DescriptionTextBox";
			this.DescriptionTextBox.Size = new System.Drawing.Size(446, 20);
			this.DescriptionTextBox.TabIndex = 6;
			// 
			// StartTimePicker
			// 
			this.StartTimePicker.Location = new System.Drawing.Point(86, 43);
			this.StartTimePicker.Name = "StartTimePicker";
			this.StartTimePicker.Size = new System.Drawing.Size(200, 20);
			this.StartTimePicker.TabIndex = 7;
			// 
			// EndTimePicker
			// 
			this.EndTimePicker.Location = new System.Drawing.Point(86, 83);
			this.EndTimePicker.Name = "EndTimePicker";
			this.EndTimePicker.Size = new System.Drawing.Size(200, 20);
			this.EndTimePicker.TabIndex = 8;
			// 
			// DatePicker
			// 
			this.DatePicker.Location = new System.Drawing.Point(86, 123);
			this.DatePicker.Name = "DatePicker";
			this.DatePicker.Size = new System.Drawing.Size(200, 20);
			this.DatePicker.TabIndex = 9;
			// 
			// EventsPanelHeader
			// 
			this.EventsPanelHeader.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.EventsPanelHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.EventsPanelHeader.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.EventsPanelHeader.ColumnCount = 1;
			this.EventsPanelHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.EventsPanelHeader.Controls.Add(this.NewEventLabel, 0, 0);
			this.EventsPanelHeader.Dock = System.Windows.Forms.DockStyle.Top;
			this.EventsPanelHeader.Location = new System.Drawing.Point(0, 0);
			this.EventsPanelHeader.Name = "EventsPanelHeader";
			this.EventsPanelHeader.RowCount = 1;
			this.EventsPanelHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
			this.EventsPanelHeader.Size = new System.Drawing.Size(541, 40);
			this.EventsPanelHeader.TabIndex = 5;
			// 
			// NewEventLabel
			// 
			this.NewEventLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.NewEventLabel.AutoSize = true;
			this.NewEventLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.NewEventLabel.Location = new System.Drawing.Point(4, 4);
			this.NewEventLabel.Margin = new System.Windows.Forms.Padding(3);
			this.NewEventLabel.MaximumSize = new System.Drawing.Size(0, 33);
			this.NewEventLabel.Name = "NewEventLabel";
			this.NewEventLabel.Size = new System.Drawing.Size(68, 33);
			this.NewEventLabel.TabIndex = 0;
			this.NewEventLabel.Text = "New event";
			this.NewEventLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CreateEventControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.EventsPanelHeader);
			this.Controls.Add(this.NewEventInputContainer);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "CreateEventControl";
			this.Size = new System.Drawing.Size(541, 409);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.NewEventInputContainer.ResumeLayout(false);
			this.NewEventInputContainer.PerformLayout();
			this.EventsPanelHeader.ResumeLayout(false);
			this.EventsPanelHeader.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button CancelButton;
		private System.Windows.Forms.Button SaveButton;
		private System.Windows.Forms.TableLayoutPanel NewEventInputContainer;
		private System.Windows.Forms.Label NewEventStartLabel;
		private System.Windows.Forms.Label NewEventEndLabel;
		private System.Windows.Forms.TableLayoutPanel EventsPanelHeader;
		private System.Windows.Forms.Label NewEventLabel;
		private System.Windows.Forms.Label DescriptionLabel;
		private System.Windows.Forms.Label DateLabel;
		private System.Windows.Forms.TextBox DescriptionTextBox;
		private System.Windows.Forms.DateTimePicker StartTimePicker;
		private System.Windows.Forms.DateTimePicker EndTimePicker;
		private System.Windows.Forms.DateTimePicker DatePicker;
	}
}
